package daos;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import models.Emp;

public class EmpDao extends AbstractDao
{

	//Method to save an emp
	public void save(Emp e) throws Exception
	{
		Connection con=getConnection();
		PreparedStatement stmt=con.prepareStatement(
				"insert into Emp (name,job,salary) values(?,?,?)");
		stmt.setString(1, e.getName());
		stmt.setString(2, e.getJob());
		stmt.setInt(3, e.getSalary());
		stmt.executeUpdate();
		con.close();
	}
	
	//Method to remove an emp
		public void remove(int id) throws Exception
		{
			Connection con=getConnection();
			PreparedStatement stmt=con.prepareStatement(
					"delete from Emp where id=?");
			stmt.setInt(1, id);
			stmt.executeUpdate();
			con.close();
		}
	
	//Method to return an emp
		public Emp findById(int id) throws Exception
		{
			Emp e=null;
			Connection con=getConnection();
			PreparedStatement stmt=con.prepareStatement(
					"select * from Emp where id=?");
			stmt.setInt(1, id);
			ResultSet rset=stmt.executeQuery();
			if(rset.next())
			{
				e=mapEmp(rset);
			}
			con.close();
			return e;
		}
	
		private Emp mapEmp(ResultSet rset) throws Exception
		{
			Emp e=new Emp();
			e.setId(rset.getInt(1));
			e.setName(rset.getString(2));
			e.setJob(rset.getString(3));
			e.setSalary(rset.getInt(4));
			return e;
		}
		//Method to return all emp
				public List<Emp> find() throws Exception
				{
					List<Emp> list=new ArrayList<Emp>();
					Connection con=getConnection();
					PreparedStatement stmt=con.prepareStatement(
							"select * from Emp");
					ResultSet rset=stmt.executeQuery();
					while(rset.next())
					{
						list.add(mapEmp(rset));
					}
					con.close();
					return list;
				}
}
