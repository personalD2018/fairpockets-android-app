package models;

import javax.xml.bind.annotation.*;

@XmlRootElement
public class Emp {

	private int id;
	private String name,job;
	private int salary;
	
	@XmlElement
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@XmlElement
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	@XmlElement
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	
}
