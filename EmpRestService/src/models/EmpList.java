package models;

import java.util.*;

import javax.xml.bind.annotation.*;


@XmlRootElement
@XmlSeeAlso(Emp.class)
public class EmpList {

	private List<Emp> empList;

	@XmlElement
	public List<Emp> getEmpList() {
		return empList;
	}

	public void setEmpList(List<Emp> empList) {
		this.empList = empList;
	}
	
	
}
