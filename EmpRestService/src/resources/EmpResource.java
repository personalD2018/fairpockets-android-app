package resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import daos.EmpDao;
import models.Emp;
import models.EmpList;

@Path("employees")
public class EmpResource {

	
	//Method to create Emp
	
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public String create(Emp e)
	{
		try
		{
			EmpDao dao=new EmpDao();
			dao.save(e);
			return "{\"msg\":\"success\"}";
		}catch(Exception ex)
		{
			System.out.println(ex);
			return ex.getMessage();
		}
	}
	
	
	@DELETE
	@Path("/{id}")
	@Produces("application/json")
	public String remove(@PathParam("id") int id)
	{
		try
		{
			EmpDao dao=new EmpDao();
			dao.remove(id);
			return "{\"msg\":\"success\"}";
		}catch(Exception ex)
		{
			System.out.println(ex);
			return ex.getMessage();
		}
	}
	
	//Method to return all Emp
	
		@GET
		@Produces("application/json")
		public EmpList fetchAll()
		{
			EmpList el=new EmpList();
			try
			{
				EmpDao dao=new EmpDao();
				List<Emp> list=dao.find();
				
				el.setEmpList(list);
				
			}catch(Exception ex)
			{
				System.out.println(ex);
				
			}
			return el;
		}
	
	
}
